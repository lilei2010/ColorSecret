I place a great deal of importance on the protection of personal information and privacy, so I have a Privacy Policy that covers how to store, use, and protect user information. We'd like to give you a clear introduction to how I handle your personal information through my privacy statement (hereafter referred to as "this Privacy Policy"). This Privacy Policy relates to your use of ColorSecret, so please read it before use And have a thorough understanding of this privacy policy and make the appropriate choices you think appropriate. If you do not agree with any of this privacy policy, you should discontinue using ColorSecret software immediately. By using the ColorSecret software, you agree and fully understand the entire contents of this Privacy Policy.

1, how to store
The data you enter is saved in your phone's local database and is not uploaded to any server.

2, how to use
We will only show you your data, do not spread to any third party, please also pay attention to your own preservation.

3, how to protect
We store your data in the APP sandbox provided by Apple and are not visible to other applications.